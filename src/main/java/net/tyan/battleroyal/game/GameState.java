package net.tyan.battleroyal.game;

/**
 * by Kevin on 23.07.2015.
 */

public enum GameState {
    LOBBY,
    PREPARING,
    INGAME,
    RESTARTING
}
