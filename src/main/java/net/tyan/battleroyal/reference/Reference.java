package net.tyan.battleroyal.reference;

/**
 * by Kevin on 23.07.2015.
 */

public class Reference {

    public static final String MYSQL_HOST = "localhost";
    public static final String MYSQL_PORT = "3306";
    public static final String MYSQL_USER = "battleroyal";
    public static final String MYSQL_PASSWORD ="123";
    public static final String MYSQL_DATABASE = "battleroyal";

    public static final String PREFIX = "§6| §eBATTLE ROYAL §6| §r";

}
