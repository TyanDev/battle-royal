package net.tyan.battleroyal.listener;

import net.tyan.battleroyal.BattleRoyal;
import net.tyan.battleroyal.reference.Reference;
import net.tyan.battleroyal.user.User;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;


/**
 * by Kevin on 24.07.2015.
 */

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        event.setQuitMessage(
            Reference.PREFIX + "§3" + event.getPlayer().getName() + " §7hat das Spiel verlassen!");

        User user = BattleRoyal.getInstance().activeUsers.get(event.getPlayer().getUniqueId());
        user.update();
        BattleRoyal.getInstance().activeUsers.remove(event.getPlayer().getUniqueId());

    }
}
