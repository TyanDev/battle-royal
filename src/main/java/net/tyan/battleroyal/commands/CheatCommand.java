package net.tyan.battleroyal.commands;

import net.tyan.battleroyal.BattleRoyal;
import net.tyan.battleroyal.scheduler.LobbyScheduler;
import net.tyan.battleroyal.user.User;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * by Kevin on 24.07.2015.
 */

public class CheatCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player))
            return true;

        Player player = (Player) sender;
        User user = BattleRoyal.getInstance().activeUsers.get(player.getUniqueId());


        if (args.length == 1) {
            int input = Integer.valueOf(args[0]);
            user.setKills(input);
            user.update();
            player.sendMessage("§6Du hast nun §3" + input + " §6Kills!");
            LobbyScheduler.time = 20;
        }

        return true;
    }

}
