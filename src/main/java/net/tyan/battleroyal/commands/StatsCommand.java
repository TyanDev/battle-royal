package net.tyan.battleroyal.commands;

import net.tyan.battleroyal.BattleRoyal;
import net.tyan.battleroyal.user.User;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * by Kevin on 24.07.2015.
 */

public class StatsCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player))
            return true;

        Player player = (Player) sender;
        User user = BattleRoyal.getInstance().activeUsers.get(player.getUniqueId());

        String prefix = "§7[§6Stats§7] ";

        String[] stats = new String[7];
        stats[0] = "§9Kills: §a" + user.getKills();
        stats[1] = "§9Tode: §a" + user.getDeaths();
        String kd = String.valueOf(user.getDeaths() > 0 ? Math.round(((double) user.getKills() / (double) user.getDeaths()) * 100.0D) / 100.0D : 0);
        if (kd.length() > 4)
            kd = kd.substring(0, 4);
        if (kd.contains("[^\\d-]"))
            kd = "0";

        stats[2] = "§9K/D: §a" + kd;
        stats[3] = "§9Punkte: §a" + user.getPoints();
        stats[4] = "§9Gespielte Runden: §a" + user.getPlayedRounds();
        stats[5] = "§9Gewonnene Runden: §a" + user.getRoundsWon();
        stats[6] = "§9Ge\u00f6ffnete Kisten: §a" + user.getChestOpened();

        for (int i = 0; i < 13; i++)
            player.sendMessage("");

        for (String stat : stats) {
            player.sendMessage(prefix + stat);
        }
        return true;
    }
}
