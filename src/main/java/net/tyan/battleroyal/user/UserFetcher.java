package net.tyan.battleroyal.user;

import net.tyan.battleroyal.BattleRoyal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * by Kevin on 23.07.2015.
 */

public class UserFetcher {
    public static boolean playerExists(String uuid) {
        boolean exists = false;

        try {
            PreparedStatement statement =
                BattleRoyal.getInstance().getMySQL().getConnection().prepareStatement(
                    "SELECT * FROM users WHERE UUID = ?;");
            statement.setString(1, uuid.replaceAll("-", ""));

            ResultSet resultSet = statement.executeQuery();

            exists = resultSet.next();
            resultSet.close();
            statement.close();

            return exists;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return exists;
        }
    }


    public static void createUser(User user) {
        String statement =
            "INSERT INTO users (UUID, Kills, Deaths, Points, PlayedRounds, RoundsWon, ChestOpened, Achievements) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

        BattleRoyal.getInstance().getMySQLHandler().new Query(statement, true, user.getCompactUUID(),
            user.getKills(), user.getDeaths(), user.getPoints(), user.getPlayedRounds(),
            user.getRoundsWon(), user.getChestOpened(), user.getAchievements());

    }
}
