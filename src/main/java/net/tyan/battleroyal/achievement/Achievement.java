package net.tyan.battleroyal.achievement;

import org.bukkit.entity.Player;

/**
 * by Kevin on 24.07.2015.
 */

public interface Achievement {

    String getName();

    String getId();

    void reach(Player player);

}
