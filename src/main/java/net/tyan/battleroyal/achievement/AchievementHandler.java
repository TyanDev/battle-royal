package net.tyan.battleroyal.achievement;

import net.tyan.battleroyal.BattleRoyal;
import net.tyan.battleroyal.achievement.achievements.OpenChestAchievement;
import net.tyan.battleroyal.reference.Messages;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.text.MessageFormat;
import java.util.*;

/**
 * by Kevin on 24.07.2015.
 */

public class AchievementHandler {

    private final List<Achievement> achievements;
    private final HashMap<UUID, List<String>> reachedAchievements;

    public AchievementHandler() {
        this.achievements = new ArrayList<>();
        this.reachedAchievements = new HashMap<>();

        registerAchievements();
    }

    public void loadReachedAchievements(UUID player) {
        ArrayList<String> reached =
            BattleRoyal.getInstance().getAchievementDataHandler().getReachedAchievements(player);
        this.reachedAchievements.put(player, reached);
    }

    public void addReachedAchievement(Player player, Achievement achievement) {

        if (!this.reachedAchievements.containsKey(player.getUniqueId())) {
            this.reachedAchievements.put(player.getUniqueId(), new ArrayList<>());
        }

        if (this.reachedAchievements.get(player.getUniqueId()).contains(achievement.getId())) {
            return;
        }

        List<String> reached = this.reachedAchievements.get(player.getUniqueId());
        reached.add(achievement.getId());
        this.reachedAchievements.put(player.getUniqueId(), reached);

        BattleRoyal.getInstance().getAchievementDataHandler().setReachedAchievements(player, reached);

        notifyReach(player, achievement);
    }

    public void notifyReach(Player player, Achievement achievement) {
        player.sendMessage(getString(achievement));
        player.playSound(player.getLocation(), Sound.LEVEL_UP, 1F, 1F);
    }

    private String getString(Achievement achievement) {
        String result = Messages.ACHIEVEMENT_REACHED;
        result = MessageFormat.format(result, achievement.getName());
        return result;
    }

    public void registerAchievement(Achievement achievement) {

        if (this.achievements.contains(achievement))
            return;
        this.achievements.add(achievement);
    }

    public void registerAchievements() {
        EnumSet<Achievements> allAchievements = EnumSet.allOf(Achievements.class);
        allAchievements
            .forEach(achievements1 -> registerAchievement(achievements1.getAchievement()));
    }

    public Achievement getAchievementByName(String name) {
        return this.achievements.stream().filter(achievement -> achievement.getName().equals(name))
            .findFirst().get();
    }

    public Achievement getAchievementByID(String id) {

        return this.achievements.stream().filter(achievement -> achievement.getId().equals(id))
            .findFirst().get();
    }

    public enum Achievements {

        OPEN_CHEST(new OpenChestAchievement());

        private final Achievement achievement;

        Achievements(Achievement achievement) {
            this.achievement = achievement;
        }

        public Achievement getAchievement() {
            return this.achievement;
        }

    }


}
