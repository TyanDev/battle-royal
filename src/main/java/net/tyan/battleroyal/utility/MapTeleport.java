package net.tyan.battleroyal.utility;

import net.tyan.battleroyal.BattleRoyal;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Random;

/**
 * by Kevin on 24.07.2015.
 */

public class MapTeleport {

    public MapTeleport() {
        teleport();
    }

    private void teleport() {
        int radius = 20;
        int players = Bukkit.getOnlinePlayers().size();
        double degree = 2 * Math.PI / players;

        Location[] locs = new Location[players];
        int counter = 0;

        for (int i = 0; i < players; i++) {

            double x = Math.cos(degree * i) * radius;
            double z = Math.sin(degree * i) * radius;

            Location middle = Bukkit.getWorld("battleroyal").getSpawnLocation();
            Location spawnPoint = middle.clone().add(x, 180, z);
            spawnPoint.clone().subtract(0, 1, 0).getBlock().setType(Material.SANDSTONE);
            Bukkit.getScheduler().scheduleSyncDelayedTask(BattleRoyal.getInstance(),
                () -> spawnPoint.clone().subtract(0, 1, 0).getBlock().setType(Material.AIR), 15L);
            locs[counter] = spawnPoint;
            counter++;
        }
        counter = 0;
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.teleport(locs[counter]);
            counter++;
        }
        Bukkit.getScheduler().scheduleSyncDelayedTask(BattleRoyal.getInstance(), () -> {
            for (Player player : Bukkit.getOnlinePlayers()) {
                Random random = new Random();
                float rnd = random.nextInt(10) + 1;
                float rnd2 = random.nextInt(10) + 1;
                player.setVelocity(new Vector(rnd, 5.25f, rnd2));
                TabActionTitle.sendTitles(player, "",
                    "Wenn du dich b\u00fcckst, schlie\u00dft du deinen Fallschirm", 4, 5, 4);
                player.setAllowFlight(true);
            }
        }, 5L);
    }
}
