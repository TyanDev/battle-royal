package net.tyan.battleroyal.utility;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

/**
 * by Kevin on 24.07.2015.
 */

public class WorldHandler {

    public static World loadWorld(String world) {
        if (!isLoaded(world))
            return Bukkit.createWorld(new WorldCreator(world));
        else
            return Bukkit.getWorld(world);
    }

    public static boolean isLoaded(String world) {
        for (World w : Bukkit.getServer().getWorlds()) {
            if (w.getName().equals(world))
                return true;
        }
        return false;
    }

    public static boolean unloadWorld(String world) {
        if (isLoaded(world)) {
            World w = Bukkit.getWorld(world);
            for (Player player : w.getPlayers())
                player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
            for (Chunk chunk : w.getLoadedChunks())
                chunk.unload();

            return Bukkit.unloadWorld(w, true);
        }
        return false;
    }

    public static void removeWorld(File directory) {
        try {
            FileUtils.deleteDirectory(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
